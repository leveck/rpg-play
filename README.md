```                                                        
 ▄▄▄▄▄  ▄▄▄▄▄    ▄▄▄         ▄▄▄▄▄  ▄        ▄▄  ▄     ▄
 █   ▀█ █   ▀█ ▄▀   ▀        █   ▀█ █        ██   ▀▄ ▄▀ 
 █▄▄▄▄▀ █▄▄▄█▀ █   ▄▄        █▄▄▄█▀ █       █  █   ▀█▀  
 █   ▀▄ █      █    █  ▀▀▀   █      █       █▄▄█    █   
 █    ▀ █       ▀▄▄▄▀        █      █▄▄▄▄▄ █    █   █   
```                                                      
                                                        
My role playing record. Everything is in the wiki.

* [RPG-Play Wiki](https://gitlab.com/leveck/rpg-play/-/wikis/home)
* [RPG-Play Gopher](gopher://1436.ninja/1Games/rpg-play.wiki)
* [RPG-Play via Gopher Proxy](https://leveck.us/1436.ninja/1Games/rpg-play.wiki)

---

Everything in this repo is licensed as ![CC-BY-NC-SA](cc.png)
[See the LICENSE file for more info](LICENSE)

